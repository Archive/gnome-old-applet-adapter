/* GNOME::OldAppletAdapter, adapter for 1.x applets
 *   Main file
 * Author: George Lebl
 * (c) 2001 Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>

#include <liboaf/liboaf.h>
#include <libgnorba/gnorba.h>

#include <bonobo.h>

#include "gnome-old-applet-adapter.h"

#include "adapter.h"
#include "panelspot.h"

#include "applet-adapter-factory-impl.h"

#include "oafids.h"

/* Some globals */
CORBA_ORB oaf_orb;
CORBA_ORB goad_orb;
PortableServer_POA goad_poa;

static GSList *panelspots = NULL;

static AppletAdapterFactoryImpl *factory = NULL;

/* UTTER HACK */
static ORBit_MessageValidationResult
accept_all_cookies (CORBA_unsigned_long request_id,
		    CORBA_Principal *principal,
		    CORBA_char *operation)
{
	/* allow ALL cookies */
	return ORBIT_MESSAGE_ALLOW_ALL;
}

static gboolean
is_cfg_used (const char *cfg)
{
	GSList *li;
	
	g_return_val_if_fail (cfg != NULL, TRUE);

	for (li = panelspots; li != NULL; li = li->next) {
		PanelSpot *spot = li->data;

		if (strcmp (spot->cfg, cfg) == 0) {
			return TRUE;
		}
	}

	return FALSE;
}

static char *
get_unique_cfg (void)
{
	char *cfg = NULL;

	for (;;) {
		int randnum = rand ();

		g_free (cfg);
		cfg = g_strdup_printf ("%sApplet_id%x/", CONFIG_DIR, randnum);

		if (g_file_exists (cfg))
			continue;

		if (is_cfg_used (cfg))
			continue;

		break;
	}

	return cfg;
}

static void
remove_from_list (PanelSpot *spot)
{
	panelspots = g_slist_remove (panelspots, spot);
}

static BonoboControl *
make_adapter (const char *goad_id, const char *cfg)
{
	PanelSpot *spot;
	BonoboControl *control;

	spot = panel_spot_new (goad_id, cfg, remove_from_list);
	panelspots = g_slist_prepend (panelspots, spot);

	control = bonobo_control_new (spot->socket);

	/* FIXME: prolly need some foo here */

	return control;
}

static BonoboControl *
create_adapter (AppletAdapterFactoryImpl *factory,
		const char *goad_id)
{
	BonoboControl *control;
	char *cfg;

	cfg = get_unique_cfg ();

	control = make_adapter (goad_id, cfg);

	g_free (cfg);

	return control;
}

static BonoboControl *
create_adapter_try_config (AppletAdapterFactoryImpl *factory,
			   const char *goad_id,
			   const char *try_config_string)
{
	BonoboControl *control;
	char *cfg;

	if (is_cfg_used (try_config_string)) {
		cfg = get_unique_cfg ();
	} else {
		cfg = g_strdup (try_config_string);
	}

	control = make_adapter (goad_id, cfg);

	g_free (cfg);

	return control;
}

static void
dump_config (AppletAdapterFactoryImpl *factory,
	     const char *config_string)
{
	/* FIXME: */
}


int
main (int argc, char *argv[])
{
	CORBA_Environment ev;

	srand (time (NULL));

	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);
	
	CORBA_exception_init (&ev);

	gnome_client_disable_master_connection ();

	/* Init GNOME and gnorba */
	goad_orb = gnome_CORBA_init_with_popt_table ("gnome-grapevine",
						     VERSION,
						     &argc, argv,
						     oaf_popt_options, 0, NULL,
						     GNORBA_INIT_SERVER_FUNC,
						     &ev);

	gdk_rgb_init ();

	/* want to accept all corba messages so we setup the request validator
	 * to just "accept all".  With Orbit 5.1 and higher this should be
	 * secure */
	ORBit_set_request_validation_handler (accept_all_cookies);

	goad_poa = (PortableServer_POA)
		CORBA_ORB_resolve_initial_references (goad_orb, "RootPOA", &ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		g_warning (_("Corba exception at %s:%d"), __FILE__, __LINE__);
		exit (1);
	}

	PortableServer_POAManager_activate (PortableServer_POA__get_the_POAManager (goad_poa, &ev), &ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		g_warning (_("Corba exception at %s:%d"), __FILE__, __LINE__);
		exit (1);
	}

	/* Initialize components and libraries we use */
	oaf_orb = oaf_init (argc, argv);
	bonobo_init (oaf_orb, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL);

	factory = applet_adapter_factory_impl_new ();
	gtk_signal_connect (GTK_OBJECT (factory), "create_adapter",
			    GTK_SIGNAL_FUNC (create_adapter),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (factory), "create_adapter_try_config",
			    GTK_SIGNAL_FUNC (create_adapter_try_config),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (factory), "dump_config",
			    GTK_SIGNAL_FUNC (dump_config),
			    NULL);

	do
		bonobo_main ();
	while (panelspots != NULL);

	bonobo_object_unref (BONOBO_OBJECT (factory));

	CORBA_exception_free (&ev);
	
	return 0;
}
