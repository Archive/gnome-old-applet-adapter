/* PanelSpot implementation
 * (C) 2001 Eazel, Inc.
 *
 * Authors:  George Lebl
 */
#ifndef PANEL_SPOT_H
#define PANEL_SPOT_H

#include "gnome-old-applet-adapter.h"

BEGIN_GNOME_DECLS

typedef struct _PanelSpot PanelSpot;
typedef struct _AppletBooter AppletBooter;

typedef void (* PanelSpotRemoved) (PanelSpot *spot);

struct _PanelSpot {
	POA_GNOME_PanelSpot servant;
        GNOME_Applet applet;

	int ref_count;

	PanelSpotRemoved remove_notify;

	char *goad_id;
	char *cfg;

	GtkWidget *socket;

	AppletBooter *booter;

	/* FIXME: implement */
	gboolean send_position;
	gboolean send_draw;
};

struct _AppletBooter {
	POA_GNOME_PanelAppletBooter servant;
	PanelSpot *spot;
};

PanelSpot *	panel_spot_new		(const char *goad_id,
					 const char *cfg,
					 PanelSpotRemoved remove_notify);

PanelSpot *	panel_spot_ref		(PanelSpot *spot);
void		panel_spot_unref	(PanelSpot *spot);

void		panel_spot_save_applet	(PanelSpot *spot);

AppletBooter *	applet_booter_new	(PanelSpot *spot);
void		applet_booter_destroy	(AppletBooter *booter);

END_GNOME_DECLS

#endif
