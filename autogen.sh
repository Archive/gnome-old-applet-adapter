#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="GNOME OldAppletAdapter"

(test -f $srcdir/configure.in \
  && test -f $srcdir/adapter.c) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gnome-old-applet-adapter directory"
    exit 1
}

. $srcdir/macros/autogen.sh
