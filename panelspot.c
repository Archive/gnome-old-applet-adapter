/* PanelSpot implementation
 * (C) 2001 Eazel, Inc.
 *
 * Authors:  George Lebl
 *
 * Portions of code:
 * (C) 1997,1998,1999,2000 the Free Software Foundation
 * (C) 2000 Eazel, Inc.
 */

#include "config.h"
#include <gnome.h>
#include <libgnorba/gnorba.h>

#include <gdk/gdkx.h>
#include <string.h>
#include <signal.h>

#include "gnome-old-applet-adapter.h"

#include "adapter.h"

#include "panelspot.h"

extern PortableServer_POA goad_poa;

static CORBA_char *
s_panelspot_get_tooltip (PortableServer_Servant servant,
			 CORBA_Environment *ev);

static void
s_panelspot_set_tooltip (PortableServer_Servant servant,
			 const CORBA_char *val,
			 CORBA_Environment *ev);

static CORBA_short
s_panelspot_get_parent_panel(PortableServer_Servant servant,
			     CORBA_Environment *ev);

static CORBA_short
s_panelspot_get_spot_pos(PortableServer_Servant servant,
			 CORBA_Environment *ev);

static GNOME_Panel_OrientType
s_panelspot_get_parent_orient(PortableServer_Servant servant,
			      CORBA_Environment *ev);

static CORBA_short
s_panelspot_get_parent_size(PortableServer_Servant servant,
			    CORBA_Environment *ev);

static CORBA_short
s_panelspot_get_free_space(PortableServer_Servant servant,
			   CORBA_Environment *ev);

static CORBA_boolean
s_panelspot_get_send_position(PortableServer_Servant servant,
			      CORBA_Environment *ev);
static void
s_panelspot_set_send_position(PortableServer_Servant servant,
			      CORBA_boolean,
			      CORBA_Environment *ev);

static CORBA_boolean
s_panelspot_get_send_draw(PortableServer_Servant servant,
			  CORBA_Environment *ev);
static void
s_panelspot_set_send_draw(PortableServer_Servant servant,
			  CORBA_boolean,
			  CORBA_Environment *ev);

static GNOME_Panel_RgbImage *
s_panelspot_get_rgb_background(PortableServer_Servant servant,
			       CORBA_Environment *ev);

static void
s_panelspot_register_us(PortableServer_Servant servant,
		     CORBA_Environment *ev);

static void
s_panelspot_unregister_us(PortableServer_Servant servant,
		       CORBA_Environment *ev);

static void
s_panelspot_abort_load(PortableServer_Servant servant,
		       CORBA_Environment *ev);

static void
s_panelspot_show_menu(PortableServer_Servant servant,
		      CORBA_Environment *ev);

static void
s_panelspot_drag_start(PortableServer_Servant servant,
		       CORBA_Environment *ev);

static void
s_panelspot_drag_stop(PortableServer_Servant servant,
		      CORBA_Environment *ev);

static void
s_panelspot_add_callback(PortableServer_Servant servant,
			 const CORBA_char *callback_name,
			 const CORBA_char *stock_item,
			 const CORBA_char *menuitem_text,
			 CORBA_Environment *ev);

static void
s_panelspot_remove_callback(PortableServer_Servant servant,
			    const CORBA_char *callback_name,
			    CORBA_Environment *ev);
static void
s_panelspot_callback_set_sensitive(PortableServer_Servant servant,
				   const CORBA_char *callback_name,
				   const CORBA_boolean sensitive,
				   CORBA_Environment *ev);

static void
s_panelspot_sync_config(PortableServer_Servant servant,
			CORBA_Environment *ev);

static void
s_panelspot_done_session_save(PortableServer_Servant servant,
			      CORBA_boolean ret,
			      CORBA_unsigned_long cookie,
			      CORBA_Environment *ev);

static PortableServer_ServantBase__epv panelspot_base_epv = {
  NULL, /* _private */
  NULL, /* finalize */
  NULL  /* use base default_POA function */
};

static POA_GNOME_PanelSpot__epv panelspot_epv = {
  NULL, /* private data */
  s_panelspot_get_tooltip,
  s_panelspot_set_tooltip,
  s_panelspot_get_parent_panel,
  s_panelspot_get_spot_pos,
  s_panelspot_get_parent_orient,
  s_panelspot_get_parent_size,
  s_panelspot_get_free_space,
  s_panelspot_get_send_position,
  s_panelspot_set_send_position,
  s_panelspot_get_send_draw,
  s_panelspot_set_send_draw,
  s_panelspot_get_rgb_background,
  s_panelspot_register_us,
  s_panelspot_unregister_us,
  s_panelspot_abort_load,
  s_panelspot_show_menu,
  s_panelspot_drag_start,
  s_panelspot_drag_stop,
  s_panelspot_add_callback,
  s_panelspot_remove_callback,
  s_panelspot_callback_set_sensitive,
  s_panelspot_sync_config,
  s_panelspot_done_session_save
};
static POA_GNOME_PanelSpot__vepv panelspot_vepv = { &panelspot_base_epv, &panelspot_epv };

/* Booter implementation */

static GNOME_PanelSpot
s_applet_booter_add_applet (PortableServer_Servant _servant,
			    const GNOME_Applet panel_applet,
			    const CORBA_char * goad_id,
			    CORBA_char ** cfgpath,
			    CORBA_char ** globcfgpath,
			    CORBA_unsigned_long * winid,
			    CORBA_Environment * ev);

static PortableServer_ServantBase__epv applet_booter_base_epv = {
  NULL, /* _private */
  NULL, /* finalize */
  NULL  /* use base default_POA function */
};

static POA_GNOME_PanelAppletBooter__epv applet_booter_epv = {
  NULL, /* private data */
  s_applet_booter_add_applet
};
static POA_GNOME_PanelAppletBooter__vepv applet_booter_vepv = { &applet_booter_base_epv, &applet_booter_epv };


/* PanelSpot object stuff */


PanelSpot *
panel_spot_ref (PanelSpot *spot)
{
	spot->ref_count++;
	return spot;
}

static void
panel_spot_destroy (PanelSpot *spot)
{
	CORBA_Environment ev;
	PortableServer_ObjectId *id;

	if (spot->remove_notify != NULL)
		spot->remove_notify (spot);
	spot->remove_notify = NULL;

	CORBA_exception_init (&ev);

	g_free (spot->goad_id);
	spot->goad_id = NULL;

	g_free (spot->cfg);
	spot->cfg = NULL;

	if (spot->socket != NULL) {
		gtk_widget_destroy (spot->socket);
		spot->socket = NULL;
	}

	CORBA_Object_release (spot->applet, &ev);
	spot->applet = NULL;
	id = PortableServer_POA_servant_to_id (goad_poa, spot, &ev);
	PortableServer_POA_deactivate_object (goad_poa, id, &ev);
	CORBA_free (id);
	POA_GNOME_PanelSpot__fini ((PortableServer_Servant) spot, &ev);
	
	CORBA_exception_free (&ev);

	g_free (spot);
}

void
panel_spot_unref (PanelSpot *spot)
{
	spot->ref_count--;
	if (spot->ref_count == 0) {
		panel_spot_destroy (spot);
	}
}

static void
socket_destroy (GtkWidget *w, gpointer data)
{
	PanelSpot *spot = data;

	spot->socket = NULL;

	panel_spot_unref (spot);
}

static void
socket_realize (GtkWidget *socket, gpointer data)
{
	CORBA_Environment ev;
	PanelSpot *spot = data;
	GNOME_Panel2 panel_client = CORBA_OBJECT_NIL;
	GNOME_PanelAppletBooter booter_client = CORBA_OBJECT_NIL;

	/* is this necessary ?? */
	if (spot->booter != NULL) {
		applet_booter_destroy (spot->booter);
		spot->booter = NULL;
	}
	spot->booter = applet_booter_new (spot);

	if (spot->booter == NULL)
		return;

	CORBA_exception_init (&ev);

	booter_client =
		PortableServer_POA_servant_to_reference (goad_poa,
							 &spot->booter->servant,
							 &ev);

	if (ev._major != CORBA_NO_EXCEPTION ||
	    booter_client == CORBA_OBJECT_NIL) {
		CORBA_exception_free (&ev);
		return;
	}

	panel_client =
		goad_server_activate_with_repo_id (NULL,
						   "IDL:GNOME/Panel2:1.0",
						   GOAD_ACTIVATE_EXISTING_ONLY,
						   NULL);
	
	if (panel_client == CORBA_OBJECT_NIL) {
		CORBA_Object_release (booter_client, &ev);
		CORBA_exception_free (&ev);
		return;
	}


	GNOME_Panel2_launch_an_applet (panel_client,
				       spot->goad_id,
				       booter_client,
				       &ev);

	CORBA_Object_release (booter_client, &ev);
	CORBA_Object_release (panel_client, &ev);

	CORBA_exception_free (&ev);
}

PanelSpot *
panel_spot_new (const char *goad_id,
		const char *cfg,
		PanelSpotRemoved remove_notify)
{
	PanelSpot *spot;
	POA_GNOME_PanelSpot *panelspot_servant;
	CORBA_Environment ev;

	spot = g_new0 (PanelSpot, 1);
	spot->ref_count = 1;
	spot->remove_notify = remove_notify;

	if (cfg == NULL || cfg[0] == '\0')
		spot->cfg = g_strconcat (CONFIG_DIR,
					 "Applet_Dummy/", NULL);
	else
		spot->cfg = g_strdup (cfg);

	CORBA_exception_init (&ev);

	panelspot_servant = (POA_GNOME_PanelSpot *)spot;
	panelspot_servant->_private = NULL;
	panelspot_servant->vepv = &panelspot_vepv;

	POA_GNOME_PanelSpot__init (panelspot_servant, &ev);
	
	CORBA_free (PortableServer_POA_activate_object (goad_poa, panelspot_servant, &ev));
	if (ev._major != CORBA_NO_EXCEPTION) {
		POA_GNOME_PanelSpot__fini (panelspot_servant, &ev);
		g_free (spot);
		CORBA_exception_free (&ev);
		return NULL;
	}

	spot->applet = CORBA_OBJECT_NIL; /* to be filled in during add_applet */
	spot->goad_id = g_strdup (goad_id);

	spot->socket = gtk_socket_new ();
	gtk_signal_connect (GTK_OBJECT (spot->socket), "destroy",
			    GTK_SIGNAL_FUNC (socket_destroy),
			    spot);
	gtk_signal_connect_after (GTK_OBJECT (spot->socket), "realize",
				  GTK_SIGNAL_FUNC (socket_realize),
				  spot);
	return spot;
}

AppletBooter *
applet_booter_new (PanelSpot *spot)
{
	AppletBooter *booter;
	POA_GNOME_PanelAppletBooter *servant;
	CORBA_Environment ev;

	g_return_val_if_fail (spot != NULL, NULL);

	booter = g_new0 (AppletBooter, 1);
	servant = (POA_GNOME_PanelAppletBooter *) booter;

	servant->_private = NULL;
	servant->vepv = &applet_booter_vepv;

	CORBA_exception_init (&ev);

	POA_GNOME_PanelSpot__init (servant, &ev);
	
	CORBA_free (PortableServer_POA_activate_object (goad_poa, servant, &ev));
	if (ev._major != CORBA_NO_EXCEPTION) {
		POA_GNOME_PanelSpot__fini (servant, &ev);
		g_free (booter);
		CORBA_exception_free (&ev);
		return NULL;
	}

	CORBA_exception_free (&ev);

	booter->spot = spot;

	return booter;
}

void
applet_booter_destroy (AppletBooter *booter)
{
	POA_GNOME_PanelAppletBooter *servant;
	CORBA_Environment ev;

	g_return_if_fail (booter != NULL);

	servant = (POA_GNOME_PanelAppletBooter *) booter;

	CORBA_exception_init (&ev);
	POA_GNOME_PanelSpot__fini (servant, &ev);
	CORBA_exception_free (&ev);

	booter->spot = NULL;
	g_free (booter);
}

/*** PanelSpot corba stuff ***/

static CORBA_char *
s_panelspot_get_tooltip(PortableServer_Servant servant,
			CORBA_Environment *ev)
{
	PanelSpot *spot = (PanelSpot *)servant;
	GtkTooltipsData *d = gtk_tooltips_data_get (spot->socket);

	if (d == NULL||
	    d->tip_text == NULL)
		return CORBA_string_dup ("");
	else
		return CORBA_string_dup (d->tip_text);
}

static void
s_panelspot_set_tooltip(PortableServer_Servant servant,
			const CORBA_char *val,
			CORBA_Environment *ev)
{
	PanelSpot *spot = (PanelSpot *)servant;
	static GtkTooltips *tooltips = NULL;

	if (tooltips == NULL)
		tooltips = gtk_tooltips_new ();

	if (val !=NULL &&
	    val[0] != '\0')
		gtk_tooltips_set_tip (tooltips, spot->socket, val, NULL);
	else
		gtk_tooltips_set_tip (tooltips, spot->socket, NULL, NULL);
}

static CORBA_short
s_panelspot_get_parent_panel (PortableServer_Servant servant,
			      CORBA_Environment *ev)
{
	/* Bogus */
	return -1;
}

static CORBA_short
s_panelspot_get_spot_pos(PortableServer_Servant servant,
			 CORBA_Environment *ev)
{
	/* Bogus */
	return -1;
}

static GNOME_Panel_OrientType
s_panelspot_get_parent_orient(PortableServer_Servant servant,
			      CORBA_Environment *ev)
{
	/* Bogus */
	return GNOME_Panel_ORIENT_UP;
}

static CORBA_short
s_panelspot_get_parent_size (PortableServer_Servant servant,
			     CORBA_Environment *ev)
{
	/* Bogus */
	/* Perhaps implement a "zooming" interface */
	return 48;
}

static CORBA_short
s_panelspot_get_free_space(PortableServer_Servant servant,
			   CORBA_Environment *ev)
{
	/* Just plain bogus */
	return 0;
}

static CORBA_boolean
s_panelspot_get_send_position (PortableServer_Servant servant,
			       CORBA_Environment *ev)
{
	PanelSpot *spot = (PanelSpot *)servant;
	
	return spot->send_position;
}

static void
s_panelspot_set_send_position(PortableServer_Servant servant,
			      CORBA_boolean enable,
			      CORBA_Environment *ev)
{
	PanelSpot *spot = (PanelSpot *)servant;
	
	spot->send_position = enable ? TRUE : FALSE;
}

static CORBA_boolean
s_panelspot_get_send_draw(PortableServer_Servant servant,
			  CORBA_Environment *ev)
{
	PanelSpot *spot = (PanelSpot *)servant;
	
	return spot->send_draw;
}

static void
s_panelspot_set_send_draw(PortableServer_Servant servant,
			  CORBA_boolean enable,
			  CORBA_Environment *ev)
{
	PanelSpot *spot = (PanelSpot *)servant;
	
	spot->send_draw = enable ? TRUE : FALSE;
}

static GNOME_Panel_RgbImage *
s_panelspot_get_rgb_background (PortableServer_Servant servant,
				CORBA_Environment *ev)
{
	/* Bogus Bogus Bogus */
	/* FIXME: This should do something mildly useful at least */
	PanelSpot *spot = (PanelSpot *)servant;
	GNOME_Panel_RgbImage *image;

	image = GNOME_Panel_RgbImage__alloc();
	image->width = spot->socket->allocation.width;
	image->height = spot->socket->allocation.height;

	/* Fake it by sending black */
	image->data._buffer = CORBA_sequence_CORBA_octet_allocbuf (3);
	image->data._length = image->data._maximum = 3;
	*(image->data._buffer) = 0;
	*(image->data._buffer+1) = 0;
	*(image->data._buffer+2) = 0;
	image->rowstride = 0;
	image->color_only = TRUE;
		
	CORBA_sequence_set_release (&image->data, TRUE);

	return image;
}

static void
s_panelspot_register_us (PortableServer_Servant servant,
			 CORBA_Environment *ev)
{
	PanelSpot *spot = (PanelSpot *)servant;
	GNOME_Panel_BackInfoType backing = {0};

	if (spot->applet == CORBA_OBJECT_NIL)
		return;

	/* initial settings, all completely bogus */
	GNOME_Applet_change_orient (spot->applet, GNOME_Panel_ORIENT_UP, ev);
	backing._d = GNOME_Panel_BACK_NONE;
	GNOME_Applet_back_change (spot->applet, &backing, ev);
	GNOME_Applet_change_size (spot->applet, 48, ev);
}

static void
s_panelspot_unregister_us (PortableServer_Servant servant,
			   CORBA_Environment *ev)
{
	PanelSpot *spot = (PanelSpot *)servant;
	if (spot->socket != NULL) {
		/* avoid races */
		GtkWidget *socket = spot->socket;
		spot->socket = NULL;
		gtk_widget_destroy (socket);
	}
}

static void
s_panelspot_abort_load (PortableServer_Servant servant,
			CORBA_Environment *ev)
{
	PanelSpot *spot = (PanelSpot *)servant;
	if (spot->socket != NULL) {
		/* avoid races */
		GtkWidget *socket = spot->socket;
		spot->socket = NULL;
		gtk_widget_destroy (socket);
	}
}

static void
s_panelspot_show_menu (PortableServer_Servant servant,
		       CORBA_Environment *ev)
{
	/* FIXME: implement menus!!!!!! */
}

static void
s_panelspot_drag_start (PortableServer_Servant servant,
			CORBA_Environment *ev)
{
	/* Not aplicable really */
}

static void
s_panelspot_drag_stop(PortableServer_Servant servant,
		      CORBA_Environment *ev)
{
	/* Not aplicable really */
}

static void
s_panelspot_add_callback (PortableServer_Servant servant,
			  const CORBA_char *callback_name,
			  const CORBA_char *stock_item,
			  const CORBA_char *menuitem_text,
			  CORBA_Environment *ev)
{
	/* FIXME: implement menus */
}

static void
s_panelspot_remove_callback (PortableServer_Servant servant,
			     const CORBA_char *callback_name,
			     CORBA_Environment *ev)
{
	/* FIXME: implement menus */
}

static void
s_panelspot_callback_set_sensitive (PortableServer_Servant servant,
				    const CORBA_char *callback_name,
				    const CORBA_boolean sensitive,
				    CORBA_Environment *ev)
{
	/* FIXME: implement menus */
}

static void
s_panelspot_sync_config(PortableServer_Servant servant,
			CORBA_Environment *ev)
{
	/* FIXME: implement session stuff */
}

void
panel_spot_save_applet (PanelSpot *spot)
{
	/* FIXME: implement session stuff */
}

static void
s_panelspot_done_session_save (PortableServer_Servant servant,
			       CORBA_boolean ret,
			       CORBA_unsigned_long cookie,
			       CORBA_Environment *ev)
{
	/* FIXME: implement session stuff */
}

/* Booter implementation */

static GNOME_PanelSpot
s_applet_booter_add_applet (PortableServer_Servant _servant,
			    const GNOME_Applet panel_applet,
			    const CORBA_char * goad_id,
			    CORBA_char ** cfgpath,
			    CORBA_char ** globcfgpath,
			    CORBA_unsigned_long * winid,
			    CORBA_Environment * ev)
{
	/* FIXME: implement booter */

	return CORBA_OBJECT_NIL;
}
